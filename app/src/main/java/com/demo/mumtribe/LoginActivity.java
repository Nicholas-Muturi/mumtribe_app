package com.demo.mumtribe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText etUsername;
    private EditText etPassword;
    private Button btn_login;
    private TextView tv_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsername = findViewById(R.id.et_LoginUsername);
        etPassword = findViewById(R.id.et_LoginPassword);
        btn_login = findViewById(R.id.btn_login);
        tv_register = findViewById(R.id.tv_register);

        btn_login.setOnClickListener(this);
        tv_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view == btn_login){
            String username = etUsername.getText().toString().trim();
            String password = etPassword.getText().toString().trim();

            if(validateData(username,password)){
                Intent intent = new Intent(this,MainActivity.class);
                startActivity(intent);
            }
            else {
                Toast.makeText(this,"Either username or password is wrong",Toast.LENGTH_SHORT).show();
            }
        }
        if(view == tv_register){
            Intent intent = new Intent(this,RegisterActivity.class);
            startActivity(intent);
        }
    }

    private boolean validateData(String username, String password){
        if(username.isEmpty()) return false;
        if(password.isEmpty()) return false;

        return true;
    }



}
