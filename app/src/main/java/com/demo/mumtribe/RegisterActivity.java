package com.demo.mumtribe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText etFirstName;
    private EditText etLastName;
    private EditText etUsername;
    private EditText etPassword;
    private EditText etConfirmPass;
    private Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etFirstName = findViewById(R.id.et_firstName);
        etLastName = findViewById(R.id.et_lastName);
        etUsername = findViewById(R.id.et_RegisterUsername);
        etPassword = findViewById(R.id.et_RegisterPassword);
        etConfirmPass = findViewById(R.id.et_confirmPassword);
        btnRegister = findViewById(R.id.btn_register);

        btnRegister.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == btnRegister){
            validateFields();
        }
    }

    private void validateFields(){
        String firstName = etFirstName.getText().toString().trim();
        String lastName = etLastName.getText().toString().trim();
        String userName = etUsername.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String confPassword = etConfirmPass.getText().toString().trim();

        if(firstName.isEmpty()){
            etFirstName.setError("Field cannot be empty");
        }
        else if(lastName.isEmpty()){
            etLastName.setError("Field cannot be empty");
        }
        else if(userName.isEmpty()){
            etUsername.setError("Field cannot be empty");
        }
        else if(password.isEmpty()){
            etPassword.setError("Field cannot be empty");
        }
        else if(confPassword.isEmpty()){
            etConfirmPass.setError("Field cannot be empty");
        }
        else if(password.length() < 6){
            etPassword.setError("Password should have min length of 6");
        }
        else if(!password.equals(confPassword)){
            etConfirmPass.setError("Password don't match");
        } else {
            // PROCEED WITH REGISTRATION . DO AWS MAGIC HERE
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

    }
}
